﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.btPaso2 = New System.Windows.Forms.Button()
        Me.txtlog = New System.Windows.Forms.TextBox()
        Me.BTSteps = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btPaso2
        '
        Me.btPaso2.BackColor = System.Drawing.Color.Red
        Me.btPaso2.ForeColor = System.Drawing.Color.White
        Me.btPaso2.Location = New System.Drawing.Point(506, 230)
        Me.btPaso2.Name = "btPaso2"
        Me.btPaso2.Size = New System.Drawing.Size(244, 40)
        Me.btPaso2.TabIndex = 0
        Me.btPaso2.Text = "BYPASS"
        Me.btPaso2.UseVisualStyleBackColor = False
        '
        'txtlog
        '
        Me.txtlog.BackColor = System.Drawing.Color.White
        Me.txtlog.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtlog.Location = New System.Drawing.Point(12, 12)
        Me.txtlog.Multiline = True
        Me.txtlog.Name = "txtlog"
        Me.txtlog.ReadOnly = True
        Me.txtlog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtlog.Size = New System.Drawing.Size(445, 485)
        Me.txtlog.TabIndex = 3
        '
        'BTSteps
        '
        Me.BTSteps.BackColor = System.Drawing.Color.Lime
        Me.BTSteps.Location = New System.Drawing.Point(506, 142)
        Me.BTSteps.Name = "BTSteps"
        Me.BTSteps.Size = New System.Drawing.Size(244, 40)
        Me.BTSteps.TabIndex = 4
        Me.BTSteps.Text = "STEPS"
        Me.BTSteps.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(566, 301)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 25)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "@iMM___27"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(800, 519)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BTSteps)
        Me.Controls.Add(Me.txtlog)
        Me.Controls.Add(Me.btPaso2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "MM PASS"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btPaso2 As Button
    Private WithEvents txtlog As TextBox
    Friend WithEvents BTSteps As Button
    Friend WithEvents Label1 As Label
End Class
