﻿Imports System.IO
Imports System.Net
Imports System.Text

Public Class c4
    Private _mensaje As String

    Public Property MENSAJE() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    Public Function Obtener(plistXML As String) As String

        Dim sPath = Application.StartupPath & "\test.txt"
        If System.IO.File.Exists(sPath) Then
            System.IO.File.Delete(sPath)
        End If

        Dim file2 As System.IO.StreamWriter
        file2 = My.Computer.FileSystem.OpenTextFileWriter(sPath, True)
        file2.WriteLine(plistXML)
        file2.Close()

        Dim responseFromServer As String = ""
        Try

            Dim requestpost = plistXML

            Dim request As WebRequest ' 
            Dim response As HttpWebResponse
            'Dim ul = GetString("UL") & "?access=" & GetString("TK")
            Dim ul = "http://localhost:9850/pass?access=EHODwxkBlwjQxbsP02MhdmQovgIX6nHD"
            request = CType(WebRequest.Create(ul), HttpWebRequest)
            request.ContentType = "application/xml"
            request.Method = "POST"

            Kill()

            request.ContentLength = requestpost.Length

            Dim requestStream As Stream = request.GetRequestStream()
            Dim byteArray As Byte() = Encoding.ASCII.GetBytes(requestpost)

            requestStream.Write(byteArray, 0, byteArray.Length)
            requestStream.Close()
            response = CType(request.GetResponse(), HttpWebResponse)

            If response.StatusDescription = "OK" Then

                Dim newStream As Stream = response.GetResponseStream()
                Dim reader As StreamReader = New StreamReader(newStream)
                responseFromServer = reader.ReadToEnd()

            End If

        Catch ex As Exception
            _mensaje = ex.Message
        End Try

        Return responseFromServer

    End Function

    Public Function GetStringP(svalue As String) As String

        Return base64_decode(My.Resources.ResourceManager.GetString(svalue))

    End Function

    Public Function GetString(svalue As String) As String

        Dim r As String
        Dim password As String = base64_decode(GetStringP("P"))
        Dim wrapper As New Sam(password)
        Dim cipherTextd As String = wrapper.DecryptData(My.Resources.ResourceManager.GetString(svalue))

        r = base64_decode(cipherTextd)

        Return r

    End Function

    Private Function base64_decode(str As String) As String

        Dim base64Decoded As String
        Dim data() As Byte
        data = System.Convert.FromBase64String(str)
        base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data)

        Return base64Decoded
    End Function

    Private Function base64_encode(str As String) As String

        Return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(str))

    End Function

    Private Sub Kill()
        Try
            For Each process As Process In Process.GetProcesses()
                If process.ProcessName = "iTunes" Then
                    process.Kill()
                End If
                If process.ProcessName = "WireShark" Then
                    process.Kill()
                End If
                If process.ProcessName = "CharlesProxy" Then
                    process.Kill()
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub

End Class
