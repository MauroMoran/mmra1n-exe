﻿Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports System.Xml
Imports CertificatesToDBandBack
Imports iMobileDevice
Imports iMobileDevice.Plist
Imports PListNet

Public Class c2
    Private _mensaje As String

    Sub New()

        Dim sUri As String = GetString("UR")

        Dim plainText As String = "aHR0cDovL21tcmFpbi5zdG9yZS9wYXNzLw=="
        Dim password As String = base64_decode(GetStringP("P"))
        Dim wrapper As New Sam(password)
        Dim cipherText As String = wrapper.EncryptData(plainText)

    End Sub

    Public Property MENSAJE() As String
        Get
            Return _mensaje
        End Get
        Set(ByVal value As String)
            _mensaje = value
        End Set
    End Property

    Public Function Obtener2(plistXML As String) As String
        Dim encodedrequest = New Xml.XmlDocument
        encodedrequest.LoadXml(plistXML)
        Dim activationDecoded = base64_decode(encodedrequest.GetElementsByTagName("data").Item(0).FirstChild.Value)
        Dim fairPlayCertChain = encodedrequest.GetElementsByTagName("data").Item(1).FirstChild.Value

        Dim decodedrequest = New Xml.XmlDocument
        decodedrequest.LoadXml(activationDecoded)
        Dim nodes = decodedrequest.GetElementsByTagName("dict").Item(0).SelectNodes("*")

        Dim activationRandomness As String
        Dim DeviceCertRequest As String
        Dim DeviceClass As String = ""
        Dim SerialNumber As String
        Dim uniqueDiviceID As String
        Dim imei As String = ""
        Dim imsi As String
        Dim iccid As String
        Dim ucid As String
        Dim ProductType As String
        Dim ActivationState As String
        Dim ProductVersion As String
        Dim MobileEquipmentIdentifier As String

        For i As Integer = 0 To nodes.Count - 1 Step 2

            Dim nodename = nodes.Item(i).FirstChild.Value
            Select Case nodename
                Case "ActivationRequestInfo"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).InnerXml
                        Select Case nodename2
                            Case "ActivationRandomness"
                                activationRandomness = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "ActivationState"
                                ActivationState = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next
                Case "DeviceInfo"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).FirstChild.Value
                        Select Case nodename2

                            Case "DeviceClass"
                                DeviceClass = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "UniqueChipID"
                                ucid = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "ProductType"
                                ProductType = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "ProductVersion"
                                ProductVersion = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next
                Case "DeviceCertRequest"
                    DeviceCertRequest = nodes.Item(i + 1).FirstChild.Value
                    Continue For
                Case "DeviceID"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).FirstChild.Value
                        Select Case nodename2
                            Case "SerialNumber"
                                SerialNumber = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "UniqueDeviceID"
                                uniqueDiviceID = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next
                Case "BasebandRequestInfo"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).InnerXml
                        Select Case nodename2
                            Case "InternationalMobileEquipmentIdentity"
                                imei = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "InternationalMobileSubscriberIdentity"
                                imsi = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "IntegratedCircuitCardIdentity"
                                iccid = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "MobileEquipmentIdentifier"
                                MobileEquipmentIdentifier = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next

            End Select

        Next

        Dim wildcardTicket = My.Computer.FileSystem.ReadAllText(Application.StartupPath & "/certs/ext/wildcardticket.txt")
        Dim accountToken =
    "{" & vbNewLine & IIf(imei <> "", """InternationalMobileEquipmentIdentity"" = """ & imei & """;", "") &
         vbNewLine & """MobileEquipmentIdentifier"" = """ & MobileEquipmentIdentifier & """;" &
            vbNewLine & """ActivityURL"" = ""https: //albert.apple.com/deviceservices/activity"";" &
                vbNewLine & """ActivationRandomness"" = """ & activationRandomness & """;" &
     vbNewLine & """UniqueDeviceID"" = """ & uniqueDiviceID & """;" &
       vbNewLine & """SerialNumber"" = """ & SerialNumber & """;" &
     vbNewLine & """CertificateURL"" = ""https://albert.apple.com/deviceservices/certifyMe"";" &
         vbNewLine & """ProductType"" = """ & ProductType & """;" &
     vbNewLine & """PhoneNumberNotificationURL"" = ""https://albert.apple.com/deviceservices/phoneHome"";" &
     vbNewLine & """WildcardTicket"" = """ & wildcardTicket & """;" &
     vbNewLine & "}"

        Dim accountTokenBase64 = base64_encode(accountToken)

        Dim devicefolder = Application.StartupPath & "\devices\" & DeviceClass & "\" & SerialNumber & "\"

        If Not System.IO.File.Exists(devicefolder) Then
            System.IO.Directory.CreateDirectory(devicefolder)
        End If

        If System.IO.File.Exists(devicefolder & "device-request.xml") Then
            System.IO.File.Delete(devicefolder & "device-request.xml")
        End If

        Dim fileok As System.IO.StreamWriter
        fileok = My.Computer.FileSystem.OpenTextFileWriter(devicefolder & "device-request.xml", True)
        fileok.WriteLine(encodedrequest)
        fileok.Close()

        If System.IO.File.Exists(devicefolder & "device-request-decoded.xml") Then
            System.IO.File.Delete(devicefolder & "device-request-decoded.xml")
        End If

        fileok = My.Computer.FileSystem.OpenTextFileWriter(devicefolder & "device-request-decoded.xml", True)
        fileok.WriteLine(decodedrequest)
        fileok.Close()

        If System.IO.File.Exists(devicefolder & "cert-request.csr") Then
            System.IO.File.Delete(devicefolder & "cert-request.csr")
        End If

        fileok = My.Computer.FileSystem.OpenTextFileWriter(devicefolder & "cert-request.csr", True)
        fileok.WriteLine(DeviceCertRequest)
        fileok.Close()

        If System.IO.File.Exists(devicefolder & "fairPlaySignature.key") Then
            System.IO.File.Delete(devicefolder & "fairPlaySignature.key")
        End If

        fileok = My.Computer.FileSystem.OpenTextFileWriter(devicefolder & "fairPlaySignature.key", True)
        fileok.WriteLine("-----BEGIN CERTIFICATE-----")
        fileok.WriteLine(fairPlayCertChain)
        fileok.WriteLine("-----END CERTIFICATE-----")
        fileok.Close()

        'Dim privkey = Array(file_get_contents("certs/original/iPhoneDeviceCA_private.key"), "minacriss")
        'Dim devicecacert = file_get_contents("certs/original/iPhoneDeviceCA.crt")

        Dim certs_path = Application.StartupPath & "\certs\"
        Dim days = 1096
        Dim certs_pass = "icloud"
        Dim cert_sn = "0x02A590E676E2CEED3A99"

        Dim signature As String

        Dim sComand = "cmd /c openssl x509 -req -sha1 -in """ & devicefolder & "cert-request.csr"" -CA " & certs_path & "iPhoneDeviceCA.crt"" -CAkey """ & certs_path & "iPhoneDeviceCA_private.key"" -out """ & devicefolder & "serverCASigned.crt"" -days " & days & " -extfile """ & certs_path & "extensions_device.cnf"" -extensions usr_cert -set_serial " & cert_sn & " -passin pass:" & certs_pass
        Shell(sComand)

        Dim accountTokenSignature = base64_encode(signature)

        plistXML = "<plist version=""1.0"">
        	<dict>
        		<key>iphone-activation</key>
        		<dict>
        			<key>activation-record</key>
        			<dict>
        				<key>FairPlayKeyData</key>
        				<data>'.$fairPlayKeyData.'</data>
        				<key>AccountTokenCertificate</key>
        				<data>'.$accountTokenCertificateBase64.'</data>
        				<key>DeviceCertificate</key>
        				<data>'.$deviceCertificate.'</data>
        				<key>AccountTokenSignature</key>
        				<data>'.$accountTokenSignature.'</data>
        				<key>AccountToken</key>
        				<data>" & accountTokenBase64 & "</data>
        			</dict>
        			<key>unbrick</key>
        			<true/>
        		</dict>
        	</dict>
        </plist>"

        Return plistXML

        'Dim val As String
        'Dim lengh As Integer
        'Dim key As String
        'Dim val2 As PlistHandle

        'Dim infoser = p.plist_dict_get_item(info, "ActivationInfoXML")
        'p.plist_get_data_val(infoser, val, lengh)

        'Dim ActivationInfoXML As String
        'Dim ActivationInfoXMLlength As String

        'val = Replace(val, "DNQRK1NKGRY9", "DNPVTUTAJCLG")

        'p.plist_to_xml(infoser, ActivationInfoXML, ActivationInfoXMLlength)

        'ActivationInfoXML = Replace(ActivationInfoXML, "DNQRK1NKGRY9", "DNPVTUTAJCLG")

        'p.plist_set_data_val(infoser, val, val.Length)

        'Dim index1 = plistXML.IndexOf("<data>")
        'Dim index2 = plistXML.IndexOf("</data>")
        'plistXML = Replace(plistXML, vbLf, "")
        'plistXML = Replace(plistXML, vbTab, "")

        'Dim datas As String = plistXML.Substring(index1 + 8, index2 - index1 - 9)


        'Dim base64Encoded As String = datas
        'Dim base64Decoded As String
        'Dim data() As Byte
        'data = System.Convert.FromBase64String(base64Encoded)
        'base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data)

        'base64Decoded = Replace(base64Decoded, "DNPVTUTAJCLG", "DNQRK1NKGRY9")

        'base64Encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(base64Decoded))

        'plistXML = Replace(plistXML, datas, base64Encoded)

    End Function
    Public Function Obtener(Optional info As PlistHandle = Nothing) As String

        Dim plistXML As String = ""

        Dim sPath = Application.StartupPath & "\test.txt"

        Dim file2 As System.IO.StreamWriter
        file2 = My.Computer.FileSystem.OpenTextFileWriter(sPath, True)
        file2.WriteLine(plistXML)
        file2.Close()

        Dim p = LibiMobileDevice.Instance.Plist

        plistXML = My.Computer.FileSystem.ReadAllText(Application.StartupPath & "\test.txt")
        Dim length As UInteger

        GoTo seguimos
        p.plist_to_xml(info, plistXML, length)
seguimos:

        Dim encodedrequest = New Xml.XmlDocument
        encodedrequest.LoadXml(plistXML)
        Dim activationDecoded = base64_decode(encodedrequest.GetElementsByTagName("data").Item(0).FirstChild.Value)

        Dim decodedrequest = New Xml.XmlDocument
        decodedrequest.LoadXml(activationDecoded)
        Dim nodes = decodedrequest.GetElementsByTagName("dict").Item(0).SelectNodes("*")

        Dim activationRandomness As String
        Dim DeviceCertRequest As String = ""
        Dim DeviceClass As String = ""
        Dim SerialNumber As String = ""
        Dim uniqueDiviceID As String
        Dim imei As String = ""
        Dim imsi As String
        Dim iccid As String
        Dim ucid As String
        Dim ProductType As String
        Dim ActivationState As String
        Dim ProductVersion As String
        Dim MobileEquipmentIdentifier As String

        For i As Integer = 0 To nodes.Count - 1 Step 2

            Dim nodename = nodes.Item(i).FirstChild.Value
            Select Case nodename
                Case "ActivationRequestInfo"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).InnerXml
                        Select Case nodename2
                            Case "ActivationRandomness"
                                activationRandomness = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "ActivationState"
                                ActivationState = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next
                Case "DeviceInfo"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).FirstChild.Value
                        Select Case nodename2

                            Case "DeviceClass"
                                DeviceClass = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "UniqueChipID"
                                ucid = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "ProductType"
                                ProductType = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "ProductVersion"
                                ProductVersion = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next
                Case "DeviceCertRequest"
                    DeviceCertRequest = nodes.Item(i + 1).FirstChild.Value
                    Continue For
                Case "DeviceID"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).FirstChild.Value
                        Select Case nodename2
                            Case "SerialNumber"
                                SerialNumber = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "UniqueDeviceID"
                                uniqueDiviceID = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next
                Case "BasebandRequestInfo"
                    Dim nodes2 = nodes.Item(i + 1).ChildNodes
                    For j As Integer = 0 To nodes2.Count - 1 Step 2
                        Dim nodename2 = nodes2.Item(j).InnerXml
                        Select Case nodename2
                            Case "InternationalMobileEquipmentIdentity"
                                imei = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "InternationalMobileSubscriberIdentity"
                                imsi = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "IntegratedCircuitCardIdentity"
                                iccid = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                            Case "MobileEquipmentIdentifier"
                                MobileEquipmentIdentifier = nodes2.Item(j + 1).FirstChild.Value
                                Continue For
                        End Select
                    Next

            End Select

        Next

        encodedrequest = New Xml.XmlDocument
        encodedrequest.LoadXml(plistXML)

        Dim ActivationInfoXML = encodedrequest.GetElementsByTagName("data").Item(0).FirstChild.Value
        Dim FairPlayCertChain = encodedrequest.GetElementsByTagName("data").Item(1).FirstChild.Value
        Dim FairPlaySignature = encodedrequest.GetElementsByTagName("data").Item(2).FirstChild.Value
        Dim RKCertification = encodedrequest.GetElementsByTagName("data").Item(3).FirstChild.Value
        Dim RKSignature = encodedrequest.GetElementsByTagName("data").Item(4).FirstChild.Value
        Dim serverKP = encodedrequest.GetElementsByTagName("data").Item(5).FirstChild.Value
        Dim signActRequest = encodedrequest.GetElementsByTagName("data").Item(6).FirstChild.Value

        ActivationInfoXML = Replace(base64_decode(ActivationInfoXML), SerialNumber, GetString("SN"))
        ActivationInfoXML = base64_encode(ActivationInfoXML)

        plistXML = "<?xml version=""1.0"" encoding=""UTF-8""?>
                    <!DOCTYPE plist PUBLIC "" -// Apple // DTD PLIST 1.0//EN"" ""http//www.apple.com/DTDs/PropertyList-1.0.dtd"">
                    <plist version=""1.0"">
        			<dict>
        				<key>ActivationInfoXML</key>
        				<data>" & ActivationInfoXML & "</data>
        				<key>FairPlayCertChain</key>
        				<data>" & FairPlayCertChain & "</data>
        				<key>FairPlaySignature</key>
        				<data>" & FairPlaySignature & "</data>
        				<key>RKCertification</key>
        				<data>" & RKCertification & "</data>
        				<key>RKSignature</key>
        				<data>" & RKSignature & "</data>
                        <key>serverKP</key>
        				<data>" & serverKP & "</data>
        				<key>signActRequest</key>
        				<data>" & signActRequest & "</data>
        			</dict>      	
                    </plist>"

        'Dim sPath = Application.StartupPath & "\test.txt"
        'If System.IO.File.Exists(sPath) Then
        '    System.IO.File.Delete(sPath)
        'End If

        'Dim file2 As System.IO.StreamWriter
        'file2 = My.Computer.FileSystem.OpenTextFileWriter(sPath, True)
        'file2.WriteLine(plistXML)
        'file2.Close()

        'POSTTTTTTTTTTTTT
        If System.IO.File.Exists(Path.GetTempPath & "\boundaryn.txt") Then
            System.IO.File.Delete(Path.GetTempPath & "\boundaryn.txt")
        End If

        Dim file3 As System.IO.StreamWriter
        file3 = My.Computer.FileSystem.OpenTextFileWriter(Path.GetTempPath & "\boundaryn.txt", True)
        file3.WriteLine("--------------------------4f1cc60f72d59a0b")
        file3.WriteLine("Content-Disposition: attachment; name=""activation-info""" & vbNewLine)
        file3.WriteLine(plistXML)
        file3.WriteLine("--------------------------4f1cc60f72d59a0b--")
        file3.Close()

        Dim requestpost = My.Computer.FileSystem.ReadAllText(Path.GetTempPath & "\boundaryn.txt")

        If System.IO.File.Exists(Path.GetTempPath & "\boundaryn.txt") Then
            System.IO.File.Delete(Path.GetTempPath & "\boundaryn.txt")
        End If

        Dim request As WebRequest ' 
        Dim response As HttpWebResponse
        Dim url2 = GetString("UR")
        request = CType(WebRequest.Create(url2), HttpWebRequest)
        request.ContentType = "multipart/form-data;; boundary=------------------------4f1cc60f72d59a0b"
        request.Method = "POST"

        request.ContentLength = requestpost.Length

        Dim requestStream As Stream = request.GetRequestStream()
        Dim byteArray As Byte() = Encoding.ASCII.GetBytes(requestpost)

        requestStream.Write(byteArray, 0, byteArray.Length)
        requestStream.Close()
        response = CType(request.GetResponse(), HttpWebResponse)

        Console.WriteLine(response.StatusDescription)

        If response.StatusDescription = "OK" Then

            Dim newStream As Stream = response.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(newStream)
            ' Leemos el contenido

            Dim responseFromServer As String = reader.ReadToEnd()

            'If System.IO.File.Exists(Application.StartupPath & "\okok.txt") Then
            '    System.IO.File.Delete(Application.StartupPath & "\okok.txt")
            'End If

            'Dim fileok As System.IO.StreamWriter
            'fileok = My.Computer.FileSystem.OpenTextFileWriter(Application.StartupPath & "\okok.txt", True)
            'fileok.WriteLine(responseFromServer)
            'fileok.Close()
            responseFromServer = My.Computer.FileSystem.ReadAllText(Application.StartupPath & "\okok.txt")
            encodedrequest = New Xml.XmlDocument
            encodedrequest.LoadXml(responseFromServer)
            'Dim activationDecoded = base64_decode(encodedrequest.GetElementsByTagName("data").Item(0).FirstChild.Value)

            Dim fairPlayKeyData = encodedrequest.GetElementsByTagName("data").Item(0).FirstChild.Value
            Dim accountTokenCertificateBase64 = encodedrequest.GetElementsByTagName("data").Item(1).FirstChild.Value
            Dim deviceCertificate = encodedrequest.GetElementsByTagName("data").Item(2).FirstChild.Value
            Dim accountTokenBase64 = encodedrequest.GetElementsByTagName("data").Item(3).FirstChild.Value
            Dim accountTokenSignature = encodedrequest.GetElementsByTagName("data").Item(4).FirstChild.Value
            accountTokenBase64 = Replace(base64_decode(accountTokenBase64), GetString("SN"), SerialNumber)
            accountTokenBase64 = base64_encode(accountTokenBase64)
            ''asdddddddddddddddddd

            If System.IO.File.Exists(Path.GetTempPath & "\boundaryn.txt") Then
                System.IO.File.Delete(Path.GetTempPath & "\boundaryn.txt")
            End If

            file3 = Nothing
            file3 = My.Computer.FileSystem.OpenTextFileWriter(Path.GetTempPath & "\boundaryn.txt", True)
            file3.WriteLine("--------------------------4f1cc60f72d59a0b")
            file3.WriteLine("Content-Disposition: attachment; name=""activation-info""" & vbNewLine)
            file3.WriteLine(accountTokenBase64)
            file3.WriteLine("--------------------------4f1cc60f72d59a0b--")
            file3.Close()

            requestpost = My.Computer.FileSystem.ReadAllText(Path.GetTempPath & "\boundaryn.txt")

            If System.IO.File.Exists(Path.GetTempPath & "\boundaryn.txt") Then
                System.IO.File.Delete(Path.GetTempPath & "\boundaryn.txt")
            End If

            url2 = "http://ec2-13-58-173-147.us-east-2.compute.amazonaws.com:8080/pass/deviceActivation.php"
            request = CType(WebRequest.Create(url2), HttpWebRequest)
            request.ContentType = "multipart/form-data;; boundary=------------------------4f1cc60f72d59a0b"
            request.Method = "POST"

            request.ContentLength = requestpost.Length

            requestStream = request.GetRequestStream()
            byteArray = Encoding.ASCII.GetBytes(requestpost)

            requestStream.Write(byteArray, 0, byteArray.Length)
            requestStream.Close()
            response = CType(request.GetResponse(), HttpWebResponse)

            Console.WriteLine(response.StatusDescription)

            If response.StatusDescription = "OK" Then
                newStream = response.GetResponseStream()
                reader = New StreamReader(newStream)
                ' Leemos el contenido

                responseFromServer = reader.ReadToEnd()
                accountTokenSignature = responseFromServer
            End If
            'asdsadas2

            If System.IO.File.Exists(Path.GetTempPath & "\boundaryn.txt") Then
                System.IO.File.Delete(Path.GetTempPath & "\boundaryn.txt")
            End If

            file3 = Nothing
            file3 = My.Computer.FileSystem.OpenTextFileWriter(Path.GetTempPath & "\boundaryn.txt", True)
            file3.WriteLine("--------------------------4f1cc60f72d59a0b")
            file3.WriteLine("Content-Disposition: attachment; name=""activation-info""" & vbNewLine)
            file3.WriteLine(base64_decode(Trim(Replace(Replace(Replace(DeviceCertRequest, " ", ""), vbTab, ""), vbLf, ""))))
            file3.WriteLine("--------------------------4f1cc60f72d59a0b--")
            file3.Close()

            requestpost = My.Computer.FileSystem.ReadAllText(Path.GetTempPath & "\boundaryn.txt")

            If System.IO.File.Exists(Path.GetTempPath & "\boundaryn.txt") Then
                System.IO.File.Delete(Path.GetTempPath & "\boundaryn.txt")
            End If

            url2 = "http://ec2-13-58-173-147.us-east-2.compute.amazonaws.com:8080/pass/deviceActivation2.php"
            request = CType(WebRequest.Create(url2), HttpWebRequest)
            request.ContentType = "multipart/form-data;; boundary=------------------------4f1cc60f72d59a0b"
            request.Method = "POST"

            request.ContentLength = requestpost.Length

            requestStream = request.GetRequestStream()
            byteArray = Encoding.ASCII.GetBytes(requestpost)

            requestStream.Write(byteArray, 0, byteArray.Length)
            requestStream.Close()
            response = CType(request.GetResponse(), HttpWebResponse)

            Console.WriteLine(response.StatusDescription)

            If response.StatusDescription = "OK" Then
                newStream = response.GetResponseStream()
                reader = New StreamReader(newStream)
                ' Leemos el contenido

                responseFromServer = reader.ReadToEnd()
                deviceCertificate = responseFromServer
            End If

            'accountTokenSignature = base64_encode(FirmarHash(accountTokenBase64, accountTokenCertificateBase64))

            plistXML = "<plist version=""1.0"">
        	<dict>
        		<key>iphone-activation</key>
        		<dict>
        			<key>unbrick</key>
        			<true/>
        			<key>activation-record</key>
        			<dict>
        				<key>FairPlayKeyData</key>
        				<data>" & fairPlayKeyData & "</data>
        				<key>AccountTokenCertificate</key>
        				<data>" & accountTokenCertificateBase64 & "</data>
        				<key>DeviceCertificate</key>
        				<data>" & deviceCertificate & "</data>
                        <key>AccountToken</key>
        				<data>" & accountTokenBase64 & "</data>
        				<key>AccountTokenSignature</key>
        				<data>" & accountTokenSignature & "</data>
        			</dict>
        		</dict>
        	</dict>
        </plist>"


        End If
        'POSTTTTTTTTTTTTT

        'plistXML = ""

        'If System.IO.File.Exists(Application.StartupPath & "\okok.txt") Then

        '    plistXML = My.Computer.FileSystem.ReadAllText(Application.StartupPath & "\okok.txt")

        'End If

        Dim _v72 = plistXML.IndexOf("<plist")

        If _v72 < 0 Then
            MENSAJE = plistXML
            Return ""
        End If

        Dim _v76 = plistXML.IndexOf("</plist>")

        If _v76 < 0 Then
            MENSAJE = plistXML
            Return ""
        End If

        Return plistXML

    End Function

    Private Function base64_decode(str As String) As String

        Dim base64Decoded As String
        Dim data() As Byte
        data = System.Convert.FromBase64String(str)
        base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data)

        Return base64Decoded
    End Function

    Private Function base64_encode(str As String) As String

        Return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(str))

    End Function


    Public Function FirmarHash(sMensaje As String, accountTokenCertificateBase64 As String) As String

        Dim cmsFirmadoBase64 As String = ""

        Dim sClaveCert As String = ""

        Dim IsValid = False
        Dim keypriv = My.Computer.FileSystem.ReadAllText(Application.StartupPath & "\signature_private.key")
        Dim cert As Certificate = New Certificate(base64_decode((accountTokenCertificateBase64)), keypriv, "")

        Dim certFirmante As X509Certificate2 = Nothing

        certFirmante = cert.GetCertificateFromPEMstring(False)

        Dim rsaObj As RSACryptoServiceProvider

        If certFirmante.HasPrivateKey Then
            rsaObj = certFirmante.PrivateKey
        End If

        ' Convierto el login ticket request a bytes, para firmar
        Dim EncodedMsg As Encoding = Encoding.UTF8
        Dim msgBytes As Byte() = EncodedMsg.GetBytes(base64_decode(sMensaje))

        Dim sha1 As New SHA1Managed
        Dim hash As Byte() = sha1.ComputeHash(msgBytes)

        Dim decryptedBytes As Byte() = rsaObj.SignHash(hash, CryptoConfig.MapNameToOID("SHA1"))

        If decryptedBytes.Length = 0 Then
            Return String.Empty
        Else
            cmsFirmadoBase64 = Convert.ToBase64String(decryptedBytes)
            Return cmsFirmadoBase64
        End If

    End Function
    Public Shared Function ObtieneCertificadoDesdeArchivo(ByVal argArchivo As String, Optional ByVal sClaveCert As String = "") As X509Certificate2
        Dim objCert As New X509Certificate2
        Try
            If sClaveCert = "" Then
                objCert.Import(My.Computer.FileSystem.ReadAllBytes(argArchivo))
            Else
                objCert.Import(My.Computer.FileSystem.ReadAllBytes(argArchivo), sClaveCert, X509KeyStorageFlags.Exportable)
            End If

            Return objCert

        Catch excepcionAlImportarCertificado As Exception
            Throw New Exception("***Error al obtener certificado: ObtieneCertificadoDesdeArchivo(" & argArchivo & "): " & excepcionAlImportarCertificado.Message & " " & excepcionAlImportarCertificado.StackTrace)
            Return Nothing
        End Try

    End Function

    Public Function GetStringP(svalue As String) As String

        Return base64_decode(My.Resources.ResourceManager.GetString(svalue))

    End Function

    Public Function GetString(svalue As String) As String

        Dim r As String
        Dim password As String = base64_decode(GetStringP("P"))
        Dim wrapper As New Sam(password)
        Dim cipherTextd As String = wrapper.DecryptData(My.Resources.ResourceManager.GetString(svalue))

        r = base64_decode(cipherTextd)

        Return r

    End Function

End Class
