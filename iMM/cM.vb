﻿Imports System.Net.Mail
Imports System.IO
Public Class cM

    Dim objSmtp As SmtpClient
    Dim objMail As MailMessage
    Private sSMTP As String
    Private sMailSalida As String
    Private sMailPsw As String
    Private sMsjError As String
    Private bCuerpoHTML As Boolean
    Dim ioReader As StreamReader

    Public ReadOnly Property MENSAJE_ERROR() As String
        Get
            Return sMsjError
        End Get
    End Property

    Public Sub New()

        objMail = New MailMessage
        objSmtp = New SmtpClient

        Try

            sSMTP = "smtp.outlook.com"
            sMailSalida = "@.com.ar"
            sMailPsw = ""

            bCuerpoHTML = True
        Catch ex As Exception

        End Try
    End Sub
    ''' <summary>
    ''' Envia Mail Con Adjunto
    ''' </summary>
    ''' <param name="sNombre"></param>
    ''' Nombre del destinatario
    ''' <param name="sPara"></param>
    ''' Correo destinatario
    ''' <param name="sAdjunto"></param>
    ''' Path del archivo adjunto
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Env(ByVal sNombre As String, ByVal strAsunto As String, ByVal sPara As String, ByVal strMensaje As String, Optional ByVal sAdjunto As String = "") As Boolean
        Dim strDestinos() As String

        Try
            objSmtp.Credentials = New Net.NetworkCredential(sMailSalida, sMailPsw)
            objSmtp.Host = sSMTP
            objSmtp.EnableSsl = True

            If objMail.Attachments.Count > 0 Then
                objMail.Attachments.Clear()
            End If

            If sAdjunto <> "" Then
                objMail.Attachments.Add(New Attachment(sAdjunto))
            End If

            If objMail.To.Count > 0 Then
                objMail.To.Clear()
            End If

            strDestinos = Split(sPara, ";")

            For i As Integer = 0 To strDestinos.Length - 1
                If strDestinos(i).IndexOf("@") > 3 Then
                    objMail.To.Add(strDestinos(i))
                End If
            Next

            If objMail.To.Count <= 0 Then Return False

            objMail.From = New MailAddress(sMailSalida, sNombre)
            objMail.Subject = strAsunto
            objMail.Body = strMensaje
            objMail.BodyEncoding = System.Text.Encoding.Default
            objMail.IsBodyHtml = bCuerpoHTML
            objSmtp.Send(objMail)

        Catch ex As Exception
            sMsjError = ex.Message
            Return False
        End Try

        Return True

    End Function

End Class
