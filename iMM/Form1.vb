﻿Imports System.Collections.ObjectModel
Imports System.IO
Imports System.Net
Imports iMobileDevice
Imports iMobileDevice.iDevice
Imports iMobileDevice.iDeviceActivation
Imports iMobileDevice.Lockdown
Imports iMobileDevice.Mobileactivation
Imports iMobileDevice.Plist
Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Sftp
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Drawing
Imports System.Linq
Imports System.Threading

Public Class Form1

    Private path As String = Application.StartupPath
    Private uid As String = ""
    Private host As String = "127.0.0.1"
    Private user As String = "root"
    Private pass As String = "alpine"
    Private proc As Process

    Public Sub Updates(ByVal strMensaje As String)

        Me.txtlog.Text += strMensaje + vbNewLine

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        NativeLibraries.Load()

        Dim oAMM As New c
        AddHandler oAMM.Estadooo, AddressOf Updates
        oAMM.MM()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btPaso2.Click

        Dim oAMM As New c
        AddHandler oAMM.Estadooo, AddressOf Updates

        oAMM.MM()

    End Sub



    Private Sub BTSteps_Click(sender As Object, e As EventArgs) Handles BTSteps.Click
        Steps()
    End Sub

    Public Sub Steps()

        Updates("Iniciando proceso...")

        If Not getDeviceUID() Then
            Updates("Error obteniendo dispositivos verifique las conexiones")
            Return
        End If

        Updates("Creando la conexion ssh")

        createCon()

        Updates("Conectando con el dispositivo")

        Dim sshclient As SshClient '= New SshClient(Me.host, Me.user, Me.pass)
        sshclient = New SshClient(Me.host, Me.user, Me.pass)
        sshclient.Connect()
        Dim scpClient As ScpClient = New ScpClient(Me.host, Me.user, Me.pass)
        scpClient.Connect()

        UploadFileScp(ScpClient, path + "\ihactivation13\bypass", "/var/mobile/Media/bypass")

        EjecutarSsh(SshClient, "mount -o rw,union,update /")
        EjecutarSsh(SshClient, "cd /var/mobile/Media")
        EjecutarSsh(SshClient, "chmod +x bypass")
        EjecutarSsh(SshClient, "./bypass")

        Updates("Steps MM finalizado")

        SshClient.Disconnect()
        ScpClient.Disconnect()
        proc.Close()


    End Sub

    Private Function getDeviceUID() As Boolean

        Dim process As Process = New Process() With {
        .StartInfo = New ProcessStartInfo() With {
        .FileName = Me.path & "\library\idevice_id.exe",
        .Arguments = "-l",
        .UseShellExecute = False,
        .RedirectStandardOutput = True,
        .CreateNoWindow = True
    }
}
        process.Start()

        process.WaitForExit()


        While Not process.StandardOutput.EndOfStream

            Dim Str As String = process.StandardOutput.ReadLine()

            If Str.Contains("Unable") Then

                Return False
            Else
                Updates(txtlog.Text + "UID: " + Str)

                Me.uid = Str
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(Me.uid)

                Return True
            End If
        End While

        Return True

    End Function

    Private Sub createCon()

        Dim startinfo = New ProcessStartInfo() With {
        .FileName = Me.path & "\library\iproxy.exe",
        .Arguments = "22 44 " + uid,
        .UseShellExecute = False,
        .RedirectStandardOutput = True,
        .CreateNoWindow = True
    }

        Me.proc = System.Diagnostics.Process.Start(startinfo)
  
    End Sub

    Private Sub EjecutarSsh(sshclient As SshClient, Command As String)
        Dim ssh As SshCommand = sshclient.CreateCommand(Command)
        Dim async As IAsyncResult = ssh.BeginExecute()
        While Not async.IsCompleted
            Thread.Sleep(2000)
            Dim result As String = ssh.EndExecute(async)
        End While
    End Sub

    Private Sub UploadFileScp(scpClient As ScpClient, info As String, path As String)

        scpClient.Upload(New FileInfo(info), path)

    End Sub

End Class
