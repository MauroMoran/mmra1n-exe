﻿Imports System.Collections.ObjectModel
Imports System.IO
Imports System.Net
Imports iMobileDevice
Imports iMobileDevice.iDevice
Imports iMobileDevice.iDeviceActivation
Imports iMobileDevice.Lockdown
Imports iMobileDevice.Mobileactivation
Imports iMobileDevice.Plist
Imports Renci.SshNet
Imports Renci.SshNet.Common
Imports Renci.SshNet.Sftp
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Drawing
Imports System.Linq
Imports System.Threading
Imports System.Text
Imports PListNet.Nodes
Imports iMobile
Imports mobiledevice

Public Class c
    Public Event Estadooo(ByVal strMensaje As String)
    Private phone As iPhone
    Private Sub LlamarEvento(ByVal strMensaje As String)
        RaiseEvent Estadooo(strMensaje)
    End Sub
    Public Sub MM()

        Try

            Dim txtlog As TextBox

            Dim iDevice = LibiMobileDevice.Instance.iDevice
            Dim Lockdown = LibiMobileDevice.Instance.Lockdown()
            Dim imobile = LibiMobileDevice.Instance.Mobileactivation
            Dim iDeviceActivation = LibiMobileDevice.Instance.iDeviceActivation
            Dim p = LibiMobileDevice.Instance.Plist

            Dim count As Integer
            Dim devices As ReadOnlyCollection(Of String)

            Dim ret = iDevice.idevice_get_device_list(devices, count)

            If ret = iDeviceError.NoDevice Then
                ' Not actually an error in our case
                Return
            End If

            If devices.Count = 0 Then
                LlamarEvento("Dispositivo no conectado")
                Exit Sub
            End If

            For i As Integer = 0 To devices.Count - 1

                Dim device As iDeviceHandle
                Dim dev = devices.Item(i)
                Dim client As LockdownClientHandle
                Dim node1 As PlistHandle
                Dim node2 As PlistHandle

                iDevice.idevice_new(device, dev).ThrowOnError()

                LlamarEvento("Conectado")

                Dim oc2 As New c4

                Lockdown.lockdownd_client_new_with_handshake(device, client, oc2.GetStringP("MD")).ThrowOnError()

                LlamarEvento("Servicio creado")

                Lockdown.lockdownd_get_value(client, Nothing, "SerialNumber", node1).ThrowOnError()

                Lockdown.lockdownd_get_value(client, Nothing, "ActivationState", node2).ThrowOnError()

                Dim SerialNumber As String = Nothing
                Dim ActivationState As String = Nothing
                Dim deviceCertRequest As String = ""

                p.plist_get_string_val(node1, SerialNumber)
                p.plist_get_string_val(node2, ActivationState)

                LlamarEvento("SerialNumber:" + SerialNumber)
                LlamarEvento("ActivationState:" + ActivationState)

                Dim service As LockdownServiceDescriptorHandle

                Lockdown.lockdownd_start_service(client, oc2.GetStringP("CO"), service).ThrowOnError()

                LlamarEvento("Servicio iniciado")

                Dim clientMobile As MobileactivationClientHandle
                imobile.mobileactivation_client_new(device, service, clientMobile).ThrowOnError()

                LlamarEvento("Servicio mobile creado")

                Dim blob As PlistHandle
                imobile.mobileactivation_create_activation_session_info(clientMobile, blob).ThrowOnError()

                Dim requestm As iDeviceActivationRequestHandle
                Dim ClientType As iDeviceActivationClientType = iDeviceActivationClientType.ClientMobileActivation

                iDeviceActivation.idevice_activation_drm_handshake_request_new(ClientType, requestm).ThrowOnError()

                iDeviceActivation.idevice_activation_request_set_fields(requestm, blob)

                Dim responsem As iDeviceActivationResponseHandle
                iDeviceActivation.idevice_activation_send_request(requestm, responsem).ThrowOnError()

                Dim fields As PlistHandle
                iDeviceActivation.idevice_activation_response_get_fields(responsem, fields)

                service.Dispose()
                Lockdown.lockdownd_start_service(client, oc2.GetStringP("CO"), service).ThrowOnError()

                clientMobile.Dispose()
                imobile.mobileactivation_client_new(device, service, clientMobile).ThrowOnError()

                Dim info As PlistHandle
                imobile.mobileactivation_create_activation_info_with_session(clientMobile, fields, info).ThrowOnError()

                service.Dispose()

                Lockdown.lockdownd_start_service(client, oc2.GetStringP("CO"), service).ThrowOnError()

                clientMobile.Dispose()

                imobile.mobileactivation_client_new(device, service, clientMobile).ThrowOnError()

                Dim plistXML As String = ""
                Dim length As UInteger

                p.plist_to_xml(info, plistXML, length)

                plistXML = oc2.Obtener(plistXML)

                If plistXML = "" Then
                    LlamarEvento(oc2.MENSAJE)
                    Exit Sub
                End If

                Dim infoxml As PlistHandle

                p.plist_from_xml(plistXML, plistXML.Length, infoxml)

                Dim getitem1 = p.plist_dict_get_item(infoxml, "iphone-activation")

                If getitem1.ToString = "0 (PlistHandle)" Then
                    getitem1 = p.plist_dict_get_item(infoxml, "device-activation")

                    If getitem1.ToString = "0 (PlistHandle)" Then
                        Exit Sub
                    End If
                End If

                Dim getitem2 = p.plist_dict_get_item(getitem1, "activation-record")

                If getitem2.ToString = "0 (PlistHandle)" Then

                    Exit Sub

                End If

                LlamarEvento("Activando dispositivo")
                imobile.mobileactivation_activate(clientMobile, getitem2).ThrowOnError()

                LlamarEvento("Activado")
                Dim PlistNewBool = p.plist_new_bool("1")
                Lockdown.lockdownd_set_value(client, Nothing, "ActivationStateAcknowledged", PlistNewBool).ThrowOnError()

                Exit Sub

            Next

        Catch ex As Exception
            LlamarEvento("Error!")
            LlamarEvento(ex.Message)
        End Try
    End Sub

End Class
